# SHRUTHI's Docs and Specs

# Guides

## Getting Started

Get started with SHRUTHI with [this guide](./getting-started.md).

## PKI Guide

Read the guide to setting up and using the [PKI](./pki.md).

# Reports

## D1.3

Read the [D1.3 report](./D1.3.md).

## D2.3

Read the [D2.3 report](./D2.3.md).