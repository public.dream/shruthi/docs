# Getting Started

To get started with SHRUTHI you will need to run the Rhyzome core API plus 2 components.

## Rhyzome API

This can be run on any host which can be accessed over the network by the hosts where the following components run. 

Follow the Install and Configuration sections in the [README](https://gitlab.com/public.dream/shruthi/rhyzome-api#install).

## Rhyzome Libvirt

This component can be run on any number of host where you want to provision instances (virtual machines or unikernels). The host(s) needs to be able to access the Rhyzome API.

Follow the Install and Configuration sections in the [README](https://gitlab.com/public.dream/shruthi/rhyzome-libvirt#install).

### Image Host

You need a basic web server where you can store VM images which is accessible by the Rhyzome Libvirt host to download from. See an example [nginx config](https://git.callpipe.com/entanglement.garden/vm-images/image-host/-/blob/master/files/nginx.conf)

## Rhyzome Openwrt

You can download a prebuilt Openwrt qcow2 image [here](https://git.callpipe.com/api/v4/projects/entanglement.garden%2Fvm-images%2Fopenwrt/jobs/artifacts/main/download?job=build) and store it in your Image Host web server.

Follow the Configuration sections in the [README](https://gitlab.com/public.dream/shruthi/rhyzome-openwrt#install).