# PKI Notes

The PKI is handled by [step](https://smallstep.com/docs/step-cli). ansible-basic will do the initial setup on managed machines,
other machines can configure step with the Entanglement CA with this command:

```
step ca bootstrap --ca-url https://ca.entanglement.garden --fingerprint ebac84b72ce1819a483e0bd80b30c35b62fc3d00e5f56f719f2be1a4d9c3ec02
```

The CA can be used to sign and verify x509 and SSH certificates.

# Generating x509 Certificates

*in ansible-managed deployments, make sure to put any certificates issued in `/etc/step/certificates` to ensure renewal takes place*

```
step ca certificate <subject> <crt-file> <key-file>
```

select the jwks provisioner key at the first prompt. You will be prompted for the password. Access the high security password vault and retreive the password:

```
ssh 172.18.100.56 cat /home/finn/jwks-pw.txt
```

that IP is the `step` VM where the actual CA key material is also held. longer term we will have more reasonable auth option, but for now a shared password is the least painful.

Note that you can issue certificates for your email address with the keycloak auth option, but domains will be rejected.

Once the password has been accepted you should have a key and certificate file. The certificate will expire in 24 hours, so make sure to renew it regularly.

# Renewing x509 certificates

certificate renewal does not require any authentication beyond the original certificate.

## Manual

```
step ca renew <crt-file> <key-file>
```

## Automatic

On a cron job, run:
```
step ca renew --force <crt-file> <key-file>
```

For a more eleborate script, [check what ansible installs](https://git.callpipe.com/entanglement.garden/ansible-basic/-/blob/master/roles/step/files/cert-renew.sh).


# SSH clients

Configure your local client with `step ssh config`. This will configure validation of signed SSH certificates by hosts. To get a client certificate to authenticate yourself
to the hosts, use `step ssh login <email>` and select the OIDC/keycloak option. Note that the email used must be the same as the email in keycloak